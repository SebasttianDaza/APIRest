import './style.css';
import {axios} from 'axios';

//import { getAllUsers, addUser, editUser, deleteUser } from "./API/CrudAjax.js";
import { getAllUsersFetch, addUserfetch, editUserFetch, deleteUserFetch } from "./API/CrudFetch.js";
import { getAllUsersAxios, addUserAxios, editUserAxios, deleteUserAxios } from "./API/CrudAxios.js";

document.querySelector("#app").innerHTML = `
  <h1>Hello Crud API Rest</h1>
  <article>
    <h2 class="title">Agregar Usuarios</h2>
    <form id="form-add">
      <div>
        <label for="name" >Nombre:</label>
        <input type="text" id="name" name="name" placeholder="Nombre" required>
      </div>
      </br>
      <div>
        <label for="cargo" >Cargo:</label>
        <input type="text" id="cargo" name="cargo" placeholder="Cargo empleado" required>
      </div>
      </br>
      <input type="submit" value="Agregar">
      <input type="hidden" id="id" name="id">
    </form>
  </article>
  <article>
    <h2>Lista de Usuarios</h2>
    <table id="table-users" border="1">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Cargo</th>
          <th>Acciones</th>
        <tr>
      </thead>
      <tbody>
          
      </tbody>
    </table>
  </article>

  <template id="template-user">
    <tr>
      <td class="name" >Juan</td>
      <td class="cargo" >Programador</td>
      <td>
        <button class="btn-edit">Editar</button>
        <button class="btn-delete">Eliminar</button>
      </td>
    </tr>
  </template>
  <a href="https://vitejs.dev/guide/features.html" target="_blank">Documentation</a>
`;



const $DOM = document,
//Table users
$table = $DOM.querySelector('#table-users'),
//Form
$form = $DOM.querySelector('#form-add'),
//Change title
$title = $DOM.querySelector('.title'),
//Template user
$template = $DOM.querySelector('#template-user').content,
//Fragment DOM
$fragment = document.createDocumentFragment();


$DOM.addEventListener("DOMContentLoaded", () => {
  //getAllUsers();
  //getAllUsersFetch($table, $template, $fragment);
  getAllUsersAxios($table, $template, $fragment);
})

$DOM.addEventListener('submit', (e) => {
  if(e.target === $form){
    e.preventDefault();
    //Si el input hidden no tiene un valor has esto
    if(!e.target.id.value){
      //Create - POST
      /*addUser({
        title: e.target.name.value,
        content: e.target.cargo.value
      });*/
      addUserfetch({
        title: e.target.name.value,
        content: e.target.cargo.value
      })
    } else {
      //Update - PUT
      /*editUser({
        title: e.target.name.value,
        content: e.target.cargo.value
      }, e.target.id.value);*/
      editUserFetch({
        title: e.target.name.value,
        content: e.target.cargo.value
      }, e.target.id.value, $table);
    } 
  }
})

$DOM.addEventListener('click', (e) => {
  if(e.target.matches(".btn-edit")) {
    $title.textContent = "Editar Usuario";
    $form.id.value = e.target.dataset.id;
    $form.name.value = e.target.dataset.name;
    $form.cargo.value = e.target.dataset.cargo;
  }
  if(e.target.matches(".btn-delete")) {
    let isDelete = confirm("¿Estas seguro de eliminar este usuario?");
    if(isDelete){
      //Delete - DELETE
      /*deleteUser(e.target.dataset.id);*/
      deleteUserFetch(e.target.dataset.id, $table);
    }
  }
});


const reusableFunction = (data, table, template, fragment) => {
  data.map((user) => {
     const $tr = template.cloneNode(true);
     $tr.querySelector(".name").textContent = user.title;
     $tr.querySelector(".cargo").textContent = user.content;
     //Almacenar el datos en atributos del elemento
     $tr.querySelector(".btn-edit").dataset.id = user.id;
     $tr.querySelector(".btn-edit").dataset.name = user.title;
     $tr.querySelector(".btn-edit").dataset.cargo = user.content;
     //Button eliminar
     $tr.querySelector(".btn-delete").dataset.id = user.id;
     fragment.appendChild($tr);
  });
   table.querySelector("tbody").appendChild(fragment);
};


const showsError = (status, table) => {
  console.log(status);
  table.insertAdjacentHTML("beforeend", `<p><b>${status}</b></p>`);
}

export { $DOM, $table, $form, $title, $template, $fragment, reusableFunction, showsError, axios};