import { reusableFunction, showsError } from "../main";


async function crudFetch({url, options, sucess, showError}) {
  try {
    const response = await fetch(url, options);
    const json = await response.json();
    sucess(json);
  }
  catch (error) {
    showError(error);
  }
}

export const getAllUsersFetch = ($table, $template, $fragment) => {
  crudFetch({
    url: "http://localhost:5555/users",
    options: {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      body: null,
    },
    sucess: (data) => {
      reusableFunction(data, $table, $template, $fragment);
    },
    showError: (error) => {
      showsError(error, $table);
    },
  });
};

export const addUserfetch = (data, $table) => {
  crudFetch({
    url: "http://localhost:5555/users",
    options: {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    },
    sucess: () => {
      location.reload();
    },
    showError: (error) => {
      showsError(error, $table);
    },
  });
}

export const editUserFetch = (data, id, $table) => {
  crudFetch({
    url: `http://localhost:5555/users/${id}`,
    options: {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    },
    sucess: () => {
      location.reload();
    },
    showError: (error) => {
      showsError(error, $table);
    },
  });
}

export const deleteUserFetch = (id, $table) => {
  crudFetch({
    url: `http://localhost:5555/users/${id}`,
    options: {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      }
    },
    sucess: () => {
      location.reload();
    },
    showError: (error) => {
      showsError(error, $table);
    },
  });
}