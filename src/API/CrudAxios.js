import { reusableFunction, showsError, axios} from "../main";



export const CrudAxios = async ({ method, url, sucess, showError, data }) => {
  try {

    const instance = axios.create({
      baseURL: url,
      headers: {
        "Content-Type": "application/json",
      },
      data: data,
    });

    const response = await instance.get({
      method: method,
    });

    const json = await response.data;
    sucess(json);
  } catch (error) {
    showError(error);
  }
};

export const getAllUsersAxios = ($table, $template, $fragment) => {
    CrudAxios({
        method: "GET",
        url: "http://localhost:5555/users",
        sucess: (data) => {
            reusableFunction(data, $table, $template, $fragment);
        },
        showError: (error) => {
            showsError(error, $table);
        },
    });
}

export const addUserAxios = (data, $table) => {
    CrudAxios({
        method: "POST",
        url: "http://localhost:5555/users",
        sucess: () => {
            location.reload();
        },
        showError: (error) => {
            showsError(error, $table);
        },
        data: data,
    });
}

export const editUserAxios = (data, id, $table) => {
    CrudAxios({
        method: "PUT",
        url: `http://localhost:5555/users/${id}`,
        sucess: () => {
            location.reload();
        },
        showError: (error) => {
            showsError(error, $table);
        },
        data: data,
    });
}

export const deleteUserAxios = (id, $table) => {
    CrudAxios({
        method: "DELETE",
        url: `http://localhost:5555/users/${id}`,
        sucess: () => {
            location.reload();
        },
        showError: (error) => {
            showsError(error, $table);
        },
    });
}
