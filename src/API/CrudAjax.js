import { $table, $template, $fragment} from '../main';


const crudAjax = ({url, method, success, error, data}) => {
    //Declaracion del objeto
    const xhr = new XMLHttpRequest();
    //Evento para readyState
    xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState !== 4) return;

        if (xhr.status >= 200 && xhr.status < 300) {
            let json = JSON.parse(xhr.responseText);
            success(json);  
        } else {
            let message = xhr.statusText||'Error'; 
            error(`${message} ${xhr.status}`);
        }
    })
    //Abrir la conexion
    xhr.open(method || "GET" , url, true);
    //Ejecutar el type content
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    //Enviar la peticion
    xhr.send(JSON.stringify(data));
}


export const getAllUsers = () => {
  crudAjax({
    method: "GET",
    url: "http://localhost:5555/users",
    success: (data) => {
      data.map((user) => {
        const $tr = $template.cloneNode(true);
        $tr.querySelector(".name").textContent = user.title;
        $tr.querySelector(".cargo").textContent = user.content;
        //Almacenar el datos en atributos del elemento
        $tr.querySelector(".btn-edit").dataset.id = user.id;
        $tr.querySelector(".btn-edit").dataset.name = user.title;
        $tr.querySelector(".btn-edit").dataset.cargo = user.content;
        //Button eliminar
        $tr.querySelector(".btn-delete").dataset.id = user.id;
        $fragment.appendChild($tr);
      });
      $table.querySelector("tbody").appendChild($fragment);
    },
    error: (status) => {
      console.log(status);
      $table.insertAdjacentHTML("beforeend", `<p><b>${status}</b></p>`);
    },
    data: null,
  });
};

export const addUser = (data) => {
  crudAjax({
    method: "POST",
    url: "http://localhost:5555/users",
    success: () => {
      location.reload();
    },
    error: (status) => {
      console.log(status);
      $table.insertAdjacentHTML("afterend", `<p><b>${status}</b></p>`);
    },
    data: data,
  });
};

export const editUser = (data, id) => {
  crudAjax({
    method: "PUT",
    url: `http://localhost:5555/users/${id}`,
    success: () => {
      location.reload();
    },
    error: (status) => {
      console.log(status);
      $table.insertAdjacentHTML("afterend", `<p><b>${status}</b></p>`);
    },
    data: data,
  });
};

export const deleteUser = (id) => {
  crudAjax({
    method: "DELETE",
    url: `http://localhost:5555/users/${id}`,
    success: () => {
      location.reload();
    },
    error: (status) => {
      console.log(status);
      $table.insertAdjacentHTML("afterend", `<p><b>${status}</b></p>`);
    },
  });
};